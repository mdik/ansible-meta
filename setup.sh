#!/bin/sh

echo "For this setup to work you need to have SSH access to a server with root privileges, and also a DNS entry that points its ip address. Do you have both of those things set up? (Y/n)"
read requirements

if [ $requirements ]; then
    rlen=${requirements%%y*}
    if [ ${#rlen} -gt 0 ]; then
        echo "i guess no, then"
        exit 1
    fi
fi

echo "What ip address is the server reachable at?"

read nextcloud_ip

echo "And what domain points to that ip addresss?"

read nextcloud_domain

echo "Checking for DNS entry..."

if res=`host $nextcloud_domain`; then
    if echo $res | grep -q "$nextcloud_ip"; then
        echo "All good!"
    else
        echo "The domain exists, but it points to a different ip address. Sometimes changing DNS entries can take several hours to update for different ISPs. Maybe try again later?"
        exit 1
    fi
else
    echo "$nextcloud_domain appears to have no DNS entries?"
    exit 1
fi

read -p "Now. To test for SSH access, we need either a combination of username and hostname, e.g. \"root@$nextcloud_ip\", or the name of the ssh-config entry: " ssh_point

ssh_domainpart=${ssh_point##*@}
ssh_userpart=${ssh_point%%@*}

if [ $ssh_domainpart = $ssh_point ]; then

    echo "Looks like manually configured ssh.\n\nWhat user are you connecting as?"
    read ssh_userpart

    echo "Ok! Trying to connect..."
    ansible_host=$ssh_point

else

    if [ $ssh_domainpart = $nextcloud_domain ]; then

        echo "Trying to connect to \"$nextcloud_domain\" as user \"$ssh_userpart\"..."
        ansible_host=$nextcloud_domain
        
    elif [ $ssh_domainpart = $nextcloud_ip ]; then

        echo "Trying to connect to \"$nextcloud_ip\" as user \"$ssh_userpart\"..."
        ansible_host=$nextcloud_ip

    else
        echo "$ssh_domainpart is neither $nextcloud_domain nor $nextcloud_ip. that is too confusing for this script :-s"
        exit 1
    fi
fi

if ssh -T $ssh_point true; then
    echo "SSH access works. Awesome!"
else
    echo "Couldn't establish SSH access to $ssh_point :-c"
    exit 1
fi

echo "This script would now create some directories and configuration files within this directory (`pwd`).\n Continue (yes/no)?"
read continue_setup

if [ $continue_setup = 'yes' ]; then

    mkdir -p "host_vars/$ansible_host"
    cat << EOF >> "host_vars/$ansible_host/all"
---

main_domain: '$nextcloud_domain'

mail_domain: '{{ main_domain }}'

nextcloud_domains: ['{{ main_domain }}']

letsencrypt_domains:
  '{{ main_domain }}': ['{{ main_domain }}']

### email address that notifications e.g. in regards to certificate renewals are sent
### please set this to a valid email address.
letsencrypt_email: 'admin@{{ mail_domain }}'
EOF

    cat << EOF >> "$ansible_host.yml"
---

- name: basic configuration for ALL the hosts
  hosts: all
  remote_user: $ssh_userpart
  become: yes

  pre_tasks:
  - name: updating the apt cache
    apt: update_cache=yes cache_valid_time=7200
    ignore_errors: true

  roles:
    - boilerplate

- name: set up $ansible_host
  hosts: $ansible_host
  remote_user: $ssh_userpart
  become: yes

  roles:
    - nextcloud
EOF

    echo "[servers]" > ./hosts
    echo "$ansible_host" >> ./hosts

else
    exit 1
fi

if [ ! -f ./roles/nextcloud/tasks/nextcloud.yml ]; then
    echo "It appears the Ansible roles have not been initialized, yet (git submodule init && git submodule update).\n Should these command be run now (yes/no)?"
read submodule_init

    if [ $submodule_init = 'yes' ]; then

        echo "Running git submodule init"
        git submodule init
        echo "Running git submodule update"
        git submodule update

    else

        echo "You'll need to get the roles either by initializing the Git submodules, or downloading them individually, but apart from that, you're..."

    fi

fi

echo "All set up! If you want to add more domains, or a specific email address to the Let's Encrypt setup, you can configure those by editing the file ./host_vars/$ansible_host/all with a text editor!"
echo ""
echo "Otherwise you can run the Ansible playbook with the command\n\n    ansible-playbook playbook.yml \n\nGood luck!"

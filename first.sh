#!/bin/sh

apt update
apt full-upgrade -y
apt install -y tmux neovim aptitude sudo

adduser --disabled-password --gecos '' admin
adduser admin sudo

cp -a ~/.ssh/ /home/admin/
chown -R admin.admin /home/admin/

update-alternatives --set editor /usr/bin/nvim

sed -i 's/%sudo\tALL=(ALL:ALL) ALL/%sudo\tALL=(ALL:ALL) NOPASSWD : ALL/' /etc/sudoers

If you'd like to install and configure a Nextcloud on a server with Debian 10 with the help of Ansible, this guide might help you getting started.

It assumes that you have SSH access to a server running Debian 10, and DNS A record that points to the server's ip address.

If you can, have a look into the Ansible tutorials on https://www.ansible.com/get-started before you start here.

Throughout this README, I will use *cloud.your-nextcloud.example* as hostname/domain for the server. Please substitude the domain you have configured for your server.


## Installing necessary command line tools

If you have a Linux operating system, like Ubuntu, Debian, or Linux Mint, you can install those tools by copy pasting

```bash
sudo apt install openssh-client git ansible
```

into a terminal, and pressing enter (potentially twice).

On MacOS, you may install those tools via [Homebrew](https://brew.sh).

On Windows, it may be most convenient to install *Ubuntu* from the Microsoft Store, and use that.


## Preparing the Server

Most of the Ansible code expects the user "admin" with sudo privileges to be present on the server. To set this user up (and also install tmux, neovim, and aptitude, as well as making neovim the default editor), you can copy `first.sh` onto the server, and run it there:

* `scp first.sh root@cloud.your-nextcloud.example:`
* `ssh root@cloud.your-nextcloud.example`
* `sh first.sh`

## Preparing your Computer

Back on you local computer, you can run

```bash
git clone https://codeberg.org/mdik/ansible-meta
cd ansible-meta
sh setup.sh
```

to run the setup script, which checks for some requirements, and, if those are met, writes the configuration files necessary for Ansible. Once those are written, you could adapt those, and also add an Ansible vault, to store all the necessary credentials in an encrypted container. If you don't do this, all necessary passwords will be auto-generated, and saved to separate files in the `credentials/` directory.


## Running the Ansible Playbook

Depending how you have set up your ssh client, or what you entered while running `setup.sh` the playbook file might be named differently, so if

```bash
ansible-playbook cloud.your-nextcloud.example.yml
```

does not work, have a look for the playbook with `ls -l *.yml`, or if that does not give any results `find . -name "*yml"`, and replace `cloud.your-nextcloud.example.yml` with the correct filename.


----


## DNS entries for mail setup

If you want to run the `dovecot` and `postfix` roles to set your server up as a mailserver (complementing the Nextcloud), you need a few more DNS entries. This example assumes that your-nextcloud.example is your *mail domain*, i.e. a possible email address could be *admin@your-nextcloud.example*, and that 1.2.3.4 is the ip address of the server we set up with Ansible.

```
@ 10800 IN MX 10 cloud.your-nextcloud.example.
@ 10800 IN TXT "v=spf1 mx a ?all"
_dmarc 10800 IN TXT "v=DMARC1; p=reject; rua=mailto:dmarc@your-nextcloud.example; adkim=s; aspf=s;"
default._domainkey 10800 IN TXT "v=DKIM1; h=sha256; k=rsa; " "p=AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA" "BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB"
cloud 10800 IN A 1.2.3.4
```

The entry for DKIM (default.\_domainkey by default) may be found in `/etc/opendkim/keys/cloud.your-nextcloud.example/default.txt` on the server, and can be copy pasted as is (the key is broken up in multiple parts because of limitations of the DNS protocol!).

Optionally you can set AAAA records for IPv6 addresses.
